extends Area2D


@onready var _timer := $Timer


func _on_body_entered(p_body: Node2D) -> void:
	print("Game Over")
	p_body.get_node("CollisionShape2D").queue_free()
	_timer.start()


func _on_timer_timeout() -> void:
	get_tree().reload_current_scene()

extends Node


var score: int = 0

@onready var _coin_label := $CoinLabel

func add_point() -> void:
	score += 1
	_coin_label.text = "Coins: " + str(score)
	print("Coins = " + str(score))

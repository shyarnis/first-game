extends CharacterBody2D


const SPEED: float = 100.0
#const JUMP_VELOCITY: float = -250.0
const JUMP_VELOCITY: float = -350.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/2d/default_gravity")

@onready var _animated_sprite := $AnimatedSprite2D


func _physics_process(p_delta: float) -> void:
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * p_delta
	
	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		
	# Get the input direction: -1, 0, 1
	var m_direction: float = Input.get_axis("move_left", "move_right")
	
	# Flip the sprite.
	if m_direction > 0:
		_animated_sprite.flip_h = false
	elif m_direction < 0:
		_animated_sprite.flip_h = true
	
	# Play animation.
	if is_on_floor():
		if m_direction == 0:
			_animated_sprite.play("idle")
		else:
			_animated_sprite.play("run")
	else:
		_animated_sprite.play("jump")
	
	
	# Apply movement.
	if m_direction:
		velocity.x = m_direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		
	move_and_slide()

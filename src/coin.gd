extends Area2D


@onready var game_manager := %GameManager
@onready var _animation_player := $AnimationPlayer


func _on_body_entered(_p_body: Node2D) -> void:
	game_manager.add_point()
	#queue_free()
	_animation_player.play("pickup")

extends Node2D


const SPEED: float = 50.0

var direction: int = 1

@onready var _animated_sprite := $AnimatedSprite2D
@onready var _ray_cast_right := $RayCastRight
@onready var _ray_cast_left := $RayCastLeft


func _process(delta: float) -> void:
	if _ray_cast_right.is_colliding():
		direction = -1
		_animated_sprite.flip_h = true
		
	if _ray_cast_left.is_colliding():
		direction = 1
		_animated_sprite.flip_h = false
		
	position.x += direction * SPEED * delta
